import Vue from "vue";
import Vuetify from "vuetify/lib/framework";

Vue.use(Vuetify);

export default new Vuetify({
    theme:{
        dark: true
    },
    breakpoints: {
        mobile: 450,
        desktop: 1250,
        ultrawide: Infinity
    },
});
