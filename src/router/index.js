import Vue from 'vue'
import VueRouter from 'vue-router'
const Details = () => import("../components/Details.vue")
const Home = () => import("../components/Home.vue")
const Favoritos = () => import ("../components/Favoritos.vue")
Vue.use(VueRouter)

const routes = [
    {
      path: '/',
      component: Home,
    },
    {
      name:'details',
      path: '/details/:id/:title',
      component: Details,
    },
    {
      name:'favoritos',
      path: '/favoritos',
      component: Favoritos
    }
  ]

const router = new VueRouter({
    routes
})

export default router