# desafio-dev-front-end

## Funcionalidades do projeto

[x]Listagem dos filmes.

[x]Informações listadas: poster e  título. Na tela de detalhes são listados mais dados, como:  sinopse, média de votos e data de publicação.

[x]Sistema de busca pelo nome do filme (input).

[x]Lista de favoritos, onde é possível visualizar os itens favoritados em uma tela específica (exibindo pôster e título).

[x]Responsividade desktop e mobile.




